# APPNAME = '{{cookiecutter.repo_name}}'
from __future__ import absolute_import, unicode_literals
import os
import sys
import environ
env = environ.Env()
environ.Env.read_env() # reading .env file

BASE_DIR = environ.Path(__file__) - 3
PROJECT_DIR = environ.Path(__file__) - 2 # ({{ cookiecutter.repo_name }}/settings/base.py - 2 = {{ cookiecutter.repo_name }}/)
APPS_DIR = PROJECT_DIR.path('apps')

APPNAME = '{{cookiecutter.repo_name}}'

# Wagtail CONFIGURATION
# ------------------------------------------------------------------------------
# LOGIN_URL = 'wagtailadmin_login'
# LOGIN_REDIRECT_URL = 'wagtailadmin_home'
WAGTAIL_SITE_NAME = '{{cookiecutter.repo_name}}'

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Raises ImproperlyConfigured exception if DJANGO_SECRET_KEY not in os.environ
SECRET_KEY = env('SECRET_KEY')
#SECRET_KEY = 'QaP(4_-z@#svsl42&*&-q3coed@)noxdkhl%*c&(ve(6tgakz)89*423!'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
ALLOWED_HOSTS = [ '*' ]

# APP CONFIGURATION
# ------------------------------------------------------------------------------
INSTALLED_APPS = [
    'admin_views',
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'whitenoise.runserver_nostatic',
    'django.contrib.staticfiles',

    'wagtail.wagtailforms',
    'wagtail.wagtailredirects',
    'wagtail.wagtailembeds',
    'wagtail.wagtailsites',
    'wagtail.wagtailusers',
    'wagtail.wagtailsnippets',
    'wagtail.wagtaildocs',
    'wagtail.wagtailimages',
    'wagtail.wagtailsearch',
    'wagtail.wagtailadmin',
    'wagtail.wagtailcore',
    'modelcluster',
    'taggit',

    'django_extensions',
    'debug_toolbar'
]

PROJECT_APPS = [
    'django_countries',
    '{{cookiecutter.repo_name}}.apps.home'
]

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS += PROJECT_APPS

# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------
MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'wagtail.wagtailcore.middleware.SiteMiddleware',
    'wagtail.wagtailredirects.middleware.RedirectMiddleware',
]

# MIGRATIONS CONFIGURATION
# ------------------------------------------------------------------------------
# MIGRATION_MODULES = {
#    'sites': '{{cookiecutter.repo_name}}.contrib.sites.migrations'
# }

# DEBUG
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
#DEBUG = env.bool('DJANGO_DEBUG', False)

# FIXTURE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
# FIXTURE_DIRS = (
#    str(APPS_DIR.path('fixtures')),
#)

# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
# EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')


# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'DIRS': [
            #'/var/www/{{cookiecutter.repo_name}}/templates',
            str(BASE_DIR.path('templates')),
        ],
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': [
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    }
]


# Needed for django-suit to work see: http://django-suit.readthedocs.io/en/develop/
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP
TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
)


# See: http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = 'bootstrap3'

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': str(BASE_DIR.path('db.sqlite3')),
    }
}
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': '{{cookiecutter.repo_name}}',
#         'USER': 'postgres',
#         'PASSWORD': '',
#         'HOST': '',  # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
#         'PORT': '',  # Set to empty string for default.
#     }
# }

# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = 'UTC'
LANGUAGE_CODE = 'en-us'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
#http://whitenoise.evans.io/en/stable/django.html
#STATIC_HOST = 'https://d4663kmspf1sqa.cloudfront.net' if not DEBUG else ''
#STATIC_URL = STATIC_HOST + '/static/'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
# UNCOMMENT: for nginx to server static files
# '/var/www/{{cookiecutter.repo_name}}/static',
STATIC_ROOT = str(BASE_DIR.path('static'))
STATIC_URL = '/static/'

# Static files (CSS, JavaScript, Images) https://docs.djangoproject.com/en/1.9/howto/static-files/
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
#
# STATICFILES_FINDERS = [
#     'django.contrib.staticfiles.finders.FileSystemFinder',
#     'django.contrib.staticfiles.finders.AppDirectoriesFinder',
# ]

# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
# UNCOMMENT: for nginx to server media files
# '/var/www/{{cookiecutter.repo_name}}/media',
MEDIA_ROOT = str(BASE_DIR.path('media'))
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = '{{cookiecutter.repo_name}}.urls'

# Python dotted path to the WSGI application used by Django's runserver.
# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = '{{cookiecutter.repo_name}}.wsgi.application'

# django-compressor
# ------------------------------------------------------------------------------
# INSTALLED_APPS += ("compressor", )
# STATICFILES_FINDERS += ("compressor.finders.CompressorFinder", )


# sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[django] %(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'stream': sys.stdout,
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# WEBPACK
# ------------------------------------------------------------------------------
# Webpack Production Stats file
#STATS_FILE = PROJECT_DIR('webpack-stats-production.json')
# Webpack config
#WEBPACK_LOADER = {
#    'DEFAULT': {
#        'BUNDLE_DIR_NAME': '{{cookiecutter.repo_name}}/static/{{cookiecutter.repo_name}}/dist/',
#        'STATS_FILE': STATS_FILE
#    }
#}
